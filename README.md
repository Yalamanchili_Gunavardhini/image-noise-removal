MATLAB application that performs image noise removal using various noise models (Gaussian, Impulse, Uniform, Exponential)
It provides access to comparison metrics such as MSE, RMSE, SNR, PSNR, and SSIM for each type of noise, displays the processed image.

Authors:
Yalamanchili Gunavardhini, Anusha V Kumar

Reference:
https://upload.wikimedia.org/wikipedia/en/thumb/7/7d/Lenna_(test_image).png/330px-Lenna_(test_image).png

License:
This license lets others distribute, remix, adapt, and build upon your work, even commercially, as long as they credit you for the original creation. This is the most accommodating of licenses offered. Recommended for maximum dissemination and use of licensed materials.

Required application: MATLAB R2023b (MATLAB 23.2)
